# Generated by Django 2.1 on 2018-09-13 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0002_auto_20180904_0915'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='title',
            field=models.CharField(default='SOME STRING', max_length=100),
        ),
    ]
