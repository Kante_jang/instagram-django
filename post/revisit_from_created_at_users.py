import pymysql
import pandas as pd
from pandas import *
import configparser
from datetime import datetime
import time
from datetime import timedelta



def minus_time(sel_time, val):
    return sel_time-timedelta(days=val)

def make_dataframe():
    config = configparser.ConfigParser()
    config.read('config.ini')

    # stage = 'DEFAULT'
    stage = 'REAL'


    password = config[stage]['SECRET_KEY'] # 'secret-key-of-myapp'
    host_name = config[stage]['AWS_DEFAULT_REGION']
    username = config[stage]['ADMIN_NAME']
    database_name = config[stage]['DB_NAME']

    db = pymysql.connect(
        host=host_name,  # DATABASE_HOST
        port=3306,
        user=username,  # DATABASE_USERNAME
        passwd=password,  # DATABASE_PASSWORD
        db=database_name,  # DATABASE_NAME
        charset='utf8'
    )

    # db 커서 초기화
    curs = db.cursor()

    ts = time.time()
    st = datetime.fromtimestamp(ts).strftime('%Y-%m-%d 15:00:00')

    select_time = st
    df1 = DataFrame(columns=('day1', 'week1', 'week2', 'week3', 'week4', 'week8', 'week12', 'week24', 'allweek'))
    df2 = DataFrame(columns=('day1_act', 'week1_act', 'week2_act', 'week3_act', 'week4_act', 'week8_act', 'week12_act', 'week24_act', 'allweek_act'))
    # df_result = DataFrame(columns=('day1', 'week1', 'week2', 'week3', 'week4', 'week8', 'week12', 'week24', 'allweek', 'day1_act', 'week1_act', 'week2_act', 'week3_act', 'week4_act', 'week8_act', 'week12_act','allweek_act', 'week24_act'))

    # sql = "select user_id, active_date, usr.created_at from active_logs join users as usr on usr.id=active_logs.user_id "
    # curs.execute(sql)
    # df_active_logs = pd.read_sql(sql, db)
    # st1 = datetime.strptime(st, "%Y-%m-%d %H:%M:%S")
    #
    # for i in range(0, 28):
    #     print("loading"+str(i))
    #     day1=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,2))& (df_active_logs.created_at <minus_time(st1,1))].groupby(["user_id"]))
    #     week1=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,8))& (df_active_logs.created_at <minus_time(st1,2))].groupby(["user_id"]))
    #     week2=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,14))& (df_active_logs.created_at <minus_time(st1,8))].groupby(["user_id"]))
    #     week3=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,21))& (df_active_logs.created_at <minus_time(st1,15))].groupby(["user_id"]))
    #     week4=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,28))& (df_active_logs.created_at <minus_time(st1,22))].groupby(["user_id"]))
    #     week8=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,56))& (df_active_logs.created_at <minus_time(st1,50))].groupby(["user_id"]))
    #     week12=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,84))& (df_active_logs.created_at <minus_time(st1,78))].groupby(["user_id"]))
    #     week24=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,168))& (df_active_logs.created_at <minus_time(st1,162))].groupby(["user_id"]))
    #     allweek=len(df_active_logs[(df_active_logs.created_at > minus_time(st1,2000))& (df_active_logs.created_at <minus_time(st1,176))])
    #     df = DataFrame(dict(day1=day1, week1=week1, week2=week2, week3=week3, week4=week4, week8=week8, week12=week12, week24=week24, allweek=allweek), index=[st1])
    #     st1 = minus_time(st1, 1)
    #     df1 = df1.append(df)

    sql = "select id, created_at from users"
    curs.execute(sql)
    df_users = pd.read_sql(sql, db)
    st1 = datetime.strptime(st, "%Y-%m-%d %H:%M:%S")

    for i in range(0, 28):
        print("loading"+str(i))
        day1=len(df_users[(df_users.created_at > minus_time(st1,2)) & (df_users.created_at<minus_time(st1,1))])
        week1=len(df_users[(df_users.created_at > minus_time(st1,7)) & (df_users.created_at<minus_time(st1,1))])
        week2=len(df_users[(df_users.created_at > minus_time(st1,14)) & (df_users.created_at<minus_time(st1,8))])
        week3=len(df_users[(df_users.created_at > minus_time(st1,21)) & (df_users.created_at<minus_time(st1,15))])
        week4=len(df_users[(df_users.created_at > minus_time(st1,28)) & (df_users.created_at<minus_time(st1,22))])
        week8=len(df_users[(df_users.created_at > minus_time(st1,56)) & (df_users.created_at<minus_time(st1,50))])
        week12=len(df_users[(df_users.created_at > minus_time(st1,84)) & (df_users.created_at<minus_time(st1,78))])
        week24=len(df_users[(df_users.created_at > minus_time(st1,168)) & (df_users.created_at<minus_time(st1,162))])
        allweek=len(df_users[(df_users.created_at > minus_time(st1,20000)) & (df_users.created_at<minus_time(st1,176))])
        df = DataFrame(dict(day1=day1, week1=week1, week2=week2, week3=week3, week4=week4, week8=week8, week12=week12, week24=week24, allweek=allweek), index=[st1])
        st1 = minus_time(st1, 1)
        df1 = df1.append(df)

    sql = "select user_id, active_date, usr.created_at from active_logs join users as usr on usr.id=active_logs.user_id "
    curs.execute(sql)
    df_active_logs = pd.read_sql(sql, db)
    st2 = datetime.strptime(st, "%Y-%m-%d %H:%M:%S")
    # curs.execute(sql)
    # df_active_logs = pd.read_sql(sql, db)

    for i in range(0, 28):
        print("loading"+str(i))
        if i==14:
            print(df_active_logs[(df_active_logs.created_at > minus_time(st2,2))& (df_active_logs.created_at <minus_time(st2,1)) & (df_active_logs.active_date == st2.date())])
        day1 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,2))& (df_active_logs.created_at <minus_time(st2,1)) & (df_active_logs.active_date == st2.date())])
        week1 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,8))& (df_active_logs.created_at <minus_time(st2,2)) & (df_active_logs.active_date == st2.date())])
        week2 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,14))& (df_active_logs.created_at <minus_time(st2,8)) & (df_active_logs.active_date == st2.date())])
        week3 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,21))& (df_active_logs.created_at <minus_time(st2,15)) & (df_active_logs.active_date == st2.date())])
        week4 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,28))& (df_active_logs.created_at <minus_time(st2,22)) & (df_active_logs.active_date == st2.date())])
        week8 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,56))& (df_active_logs.created_at <minus_time(st2,50)) & (df_active_logs.active_date == st2.date())])
        week12 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,84))& (df_active_logs.created_at <minus_time(st2,78)) & (df_active_logs.active_date == st2.date())])
        week24 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,168))& (df_active_logs.created_at <minus_time(st2,162)) & (df_active_logs.active_date == st2.date())])
        allweek = len(df_active_logs[(df_active_logs.created_at > minus_time(st2,20000))& (df_active_logs.created_at <minus_time(st2,176)) & (df_active_logs.active_date == st2.date())])
        df = DataFrame(dict(day1_act=day1, week1_act=week1, week2_act=week2, week3_act=week3, week4_act=week4, week8_act=week8, week12_act=week12, week24_act=week24, allweek_act=allweek), index=[st2])
        st2 = minus_time(st2, 1)
        df2 = df2.append(df)

    df_result = pd.concat([df1,df2], axis=1)
    df_result = df_result.reindex(['day1','day1_act','week1','week1_act','week2','week2_act','week3','week3_act','week4','week4_act','week8','week8_act','week12','week12_act','week24','week24_act','allweek','allweek_act'], axis =1)
    # print(df_result)
    # print(df1)
    # df_result.to_html('templates/include/pd_html.html')
    # df_result.to_csv("output.csv", sep=',', encoding='utf-8')

    db.close()
    return df_result
