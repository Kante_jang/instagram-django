from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from . import views

from .api import PostViewSet

app_name = 'post'

router = routers.DefaultRouter()
router.register('post', PostViewSet)

from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^create/$', views.post_create, name='post_create'),
    url(r'^product/$', views.productView, name='product'),
    url(r'^export/$', views.export_csv, name='export_csv') ,
    url(r'^(?P<post_pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^(?P<post_pk>\d+)/comment/create/$', views.comment_create, name='comment_create'),
    url(r'^add/', views.post_add, name='post_add'),
    url(r'^(?P<pk>\d+)/delete/$', views.post_delete, name='post_delete'),
    url(r'^api/doc$', get_swagger_view(title='Rest API Document')),
    url(r'^api/v1/', include((router.urls, 'post'), namespace='api')),
]