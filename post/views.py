from django.http import HttpResponse, StreamingHttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from .forms import CommentForm
from .models import Post
from django.contrib import messages
from .forms import PostForm
from django.contrib.auth.decorators import login_required
import pandas as pd
import io
from .revisit_from_created_at_users import make_dataframe
from django.contrib.auth import get_user_model
import csv
from django.contrib.auth.models import User


def home_view(request):
    posts = Post.objects.all()
    context = {
        'posts': posts,
    }
    return render(request, 'base_home.html', context)



def post_list(request):
    posts = Post.objects.all()
    context = {
        'posts': posts,
    }
    return render(request, 'post/post_list.html', context)


def post_detail(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment_form = CommentForm()
    context = {
        'post': post,
        'comment_form': comment_form,
    }
    return render(request, 'post/post_detail.html', context)

# def comment_create(request, post_pk):
#     # 요청 메서드가 POST방식 일 때만 처리
#     if request.method == 'POST':
#         # Post인스턴스를 가져오거나 404 Response를 돌려줌
#         post = get_object_or_404(Post, pk=post_pk)
#         # request.POST에서 'content'키의 값을 가져옴
#         comment_form = CommentForm(request.POST)
#         # 올바른 데이터가 Form인스턴스에 바인딩 되어있는지 유효성 검사
#         if comment_form.is_valid():
#             # 유효성 검사에 통과하면 Comment객체 생성 및 DB저장
#             Comment.objects.create(
#                 post=post,
#                 # 작성자는 현재 요청의 사용자로 지정
#                 author=request.user,
#                 content=comment_form.cleaned_data['content']
#             )
#         # 정상적으로 Comment가 생성된 후
#         # 'post'네임스페이스를 가진 url의 'post_list'이름에 해당하는 뷰로 이동
#         return redirect('post:post_list')


@login_required
def comment_create(request, post_pk):
    # GET파라미터로 전달된 작업 완료 후 이동할 URL값
    next_path = request.GET.get('post/post_detail.html')

    # 요청 메서드가 POST방식 일 때만 처리
    if request.method == 'POST':
        # Post인스턴스를 가져오거나 404 Response를 돌려줌
        post = get_object_or_404(Post, pk=post_pk)
        # request.POST데이터를 이용한 Bounded Form생성
        comment_form = CommentForm(request.POST)
        # 올바른 데이터가 Form인스턴스에 바인딩 되어있는지 유효성 검사
        if comment_form.is_valid():
            # 유효성 검사에 통과하면 ModelForm의 save()호출로 인스턴스 생성
            # DB에 저장하지 않고 인스턴스만 생성하기 위해 commit=False옵션 지정
            comment = comment_form.save(commit=False)
            # CommentForm에 지정되지 않았으나 필수요소인 author와 post속성을 지정
            comment.post = post
            comment.author = request.user
            # DB에 저장
            comment.save()

            # 성공 메시지를 다음 request의 결과로 전달하도록 지정
            messages.success(request, '댓글이 등록되었습니다')
        else:
            # 유효성 검사에 실패한 경우
            # 에러 목록을 순회하며 에러메시지를 작성, messages의 error레벨로 추가
            error_msg = '댓글 등록에 실패했습니다\n{}'.format(
                '\n'.join(
                    [f'- {error}'
                     for key, value in comment_form.errors.items()
                     for error in value]))
            messages.error(request, error_msg)

        # next parameter에 값이 담겨 온 경우, 해당 경로로 이동
        if next_path:
            return redirect(next_path)
        # next parameter가 빈 경우 post_list뷰로 이동
        return redirect('post:post_list')


# @login_required
# def post_create(request):
#     if request.method == 'POST':
#         # PostForm은 파일을 처리하므로 request.FILES도 함께 바인딩
#         post_form = PostForm(request.POST, request.FILES)
#         if post_form.is_valid():
#             User = get_user_model()
#             print(User.objects.first)
#             print("@@@@@@@@@@@1")
#             author = User.objects.get(username='kante')
#             title = request.POST.get('title','TLSLDA')
#             print("@@@@@@@@@@@1")
#             content = request.POST ['content']
#             print("@@@@@@@@@@@2")
#             # author = User.objects.get(username=request.user)
#             # author필드를 채우기 위해 인스턴스만 생성
#             post = Post.objects.create(
#                 # Post 객체의 author 필드에 'author' 변수 할당
#                 author=author,
#                 title=title,
#                 content=content,
#             )
#             print(post.title)
#             print("@@@@@@@@@@@3")
#             # author필드를 채운 후 DB에 저장
#             post.save()
#             print("@@@@@@@@@@@3")
#
#             # 성공 알림을 messages에 추가 후 post_list뷰로 이동
#             messages.success(request, '사진이 등록되었습니다')
#             return redirect('post:post_list')
#     else:
#         post_form = PostForm()
#
#     context = {
#         'post_form': post_form,
#     }
#     return render(request, 'post/post_create.html', context)


def productView(request):
    df = make_dataframe()
    # df = pd.DataFrame([1000, 2000, 3000, 4000], index=["i1", "i2", "i3", "i4"])
    # df.to_html
    # qs = Product.pdobjects.all()  # Use the Pandas Manager
    # df = qs.to_dataframe()
    template = 'post/product.html'
    # Format the column headers for the Bootstrap table, they're just a list of field names,
    # duplicated and turned into dicts like this: {'field': 'foo', 'title: 'foo'}
    # columns = [{'field': f, 'title': f} for f in df]
    # Write the DataFrame to JSON (as easy as can be)
    # json = df.to_json(orient='records')  # output just the records (no fieldnames) as a collection of tuples
    # Proceed to create your context object containing the columns and the data
    # context = {
    #          'data': json,
    #          'columns': columns
    #         }
    # And render it!
    return render(request, template, {'context' : df.to_html })


def export_csv(request, df_input):
    sio = io.StringIO
    df = df_input
    PandasWriter = pd.ExcelWriter(sio, engine = 'xlswriter')
    df.to_excel(PandasWriter, sheet_name='pdf')
    PandasWriter.save()

    sio.seek(0)
    workbook = sio.getvalue()

    response = StreamingHttpResponse(workbook, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % 'output.csv'
    return response


# @login_required
# def post_add(request):
#     print("@@@@@@@@@   start    @@@@@@@@@@@@@@@@")
#     if request.method == 'POST':
#         print("@@@@@@@@@   0    @@@@@@@@@@@@@@@@")
#         post_form = PostForm(request.FILES)
#         print("@@@@@@@@@   1    @@@@@@@@@@@@@@@@")
#         # if post_form.is_valid():
#         print("@@@@@@@@@   2    @@@@@@@@@@@@@@@@")
#         User = get_user_model()
#         print("@@@@@@@@@   3    @@@@@@@@@@@@@@@@")
#         author = User.objects.get(username='kante')
#         print("@@@@@@@@@   4   @@@@@@@@@@@@@@@@")
#         title = request.POST['title']
#         print("@@@@@@@@@   5    @@@@@@@@@@@@@@@@")
#         content = request.POST['content']
#         print("@@@@@@@@@   6    @@@@@@@@@@@@@@@@")
#         context = {
#             'author' : author,
#             'title' : title,
#             'post_form': post_form,
#             'content' : content,
#         }
#         post = Post.objects.create(
#             photo = context['post_form'],
#             author = context['author'],
#             title = context['title'],
#             content = context['content'],
#         )
#
#         print("@@@@@@@@@   8    @@@@@@@@@@@@@@@@")
#         post.save()
#         messages.success(request, '사진이 등록되었습니다')
#         return render(request, 'post/post_add.html', context)
#     elif request.method == 'GET':
#         return render(request, 'post/post_add.html')

@login_required
def post_add(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():

            User = get_user_model()
            print(request.user.password)
            post = form.save(commit=False)
            post.title = request.POST['title']
            post.content = request.POST['content']
            post.author = User.objects.get(username = request.user.username)
            post.save()
            messages.success(request, '새 글이 등록되었습니다.')
            return redirect('post:post_list')

    else:
        form = PostForm()
    return render(request, 'post/post_add.html', {
        'form': form,
    })

@login_required
def post_create(request):
    if request.method == 'POST':
        post_form = PostForm(request.FILES)
        if post_form.is_valid():
            User = get_user_model()
            user_id = int(request.POST['id'])
            user = User.objects.get(id=user_id)
            print(user)
            author = User.objects.get(username='kante')
            title = request.POST['title']
            print(title.name)
            content = request.POST['content']
            post = Post.objects.create(
                author=author,
                title=title,
                content=content,
            )
            print(post.title)


            messages.success(request, '사진이 등록되었습니다')
            context = {
                'post_form': post_form,
            }
            post.save()
            return render(request, 'post/post_create.html', context)
    else:
        post_form = PostForm()

    context = {
        'post_form': post_form,
    }
    return render(request, 'post/post_create.html', context)


def post_delete(request, pk):
    if request.method == 'POST':
        print(pk)
        post = Post.objects.get(pk=pk)
        post.delete()
        return render(request, 'post/post_delete.html')

    elif request.method == 'GET':
        return HttpResponse('잘못된 접근 입니다.')