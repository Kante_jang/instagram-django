import pymysql
import pandas as pd
import time
from pandas import Series, DataFrame
import configparser
import datetime, time
import numpy as np
import csv

config = configparser.ConfigParser()
config.read('config.ini')

# stage = 'DEFAULT'
stage = 'REAL'


password = config[stage]['SECRET_KEY'] # 'secret-key-of-myapp'
host_name = config[stage]['AWS_DEFAULT_REGION']
username = config[stage]['ADMIN_NAME']
database_name = config[stage]['DB_NAME']


db = pymysql.connect(
    host=host_name,  # DATABASE_HOST
    port=3306,
    user=username,  # DATABASE_USERNAME
    passwd=password,  # DATABASE_PASSWORD
    db=database_name,  # DATABASE_NAME
    charset='utf8'
)

# week1 = len(df_active_logs[(df_active_logs.created_at > minus_time(st2, 8)) & (df_active_logs.created_at < minus_time(st2, 2)) & (df_active_logs.active_date == st2.date())])

curs = db.cursor()


sql = "select user_id, pst.name from post_hits left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id limit 50000"
curs.execute(sql)
df_styles = pd.read_sql(sql,db)
k = (1,31,37)
df_styles = df_styles[df_styles['user_id'].isin(k)]
df_styles['id']=df_styles.index
# print(df_styles)
sql = " select pst.name, count(pst.name) from post_hits left join (select post_id, style_id, st.name from post_styles join styles as st on st.id= post_styles.style_id) as pst on pst.post_id = post_hits.post_id where user_id in (1,31,37) group by pst.name"
curs.execute(sql)
df_styles = pd.read_sql(sql,db)
# print(df_styles)
# df_styles['id']=df_styles.index
# print(df_styles.groupby(['name']).sum())
dfa = df_styles.groupby(['name']).sum()

dfa = dfa.reset_index()
print(dfa)
# .groupby(['name']).sum()