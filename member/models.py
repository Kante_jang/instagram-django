from django.contrib.auth.models import AbstractUser


#커스텀 유저 모델을 정의할 때는 AbstractUser 또는 AbstractBaseUser 클래스를 상속받는다.

class User(AbstractUser):
    pass